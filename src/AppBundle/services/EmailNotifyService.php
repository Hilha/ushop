<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27/02/19
 * Time: 13:51
 */

namespace AppBundle\services;


use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class EmailNotifyService

{
    /** @var \Twig_Environment $twig */
    private $twig;

    /** @var \Swift_Mailer $mailer */
    private $mailer;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var TranslatorInterface $translator */
    private $translator;

    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    public function notifyLowStock(){
        /** @var ProductRepository $productRep */
        $productRep = $this->entityManager->getRepository(Product::class);
        $lowStockProducts = $productRep->findByStockQtd(12);

        if(!empty($lowStockProducts)){
            /** @var UserRepository $usersRep */
            $usersRep = $this->entityManager->getRepository(User::class);
            $users = $usersRep->findByRole('ROLE_GERENCIAR_PRODUTOS', 'ROLE_ADMIN');
            foreach ($users as $user) {
                $message = (new \Swift_Message($this->translator->trans('email.title',[],'AppBundle')))
                    ->setFrom('matheushilhateste@gmail.com')
                    ->setTo($user->getEmail())
                    ->setBody(
                      $this->twig->render(
                          '@App/emails/lowstock.html.twig',
                          ['products'=>$lowStockProducts]
                      ),
                        'text/html'
                    );
                $this->mailer->send($message);
            }
        }

    }
}