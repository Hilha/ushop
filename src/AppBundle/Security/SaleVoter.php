<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28/02/19
 * Time: 15:43
 */

namespace AppBundle\Security;



use AppBundle\Entity\Sale;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SaleVoter extends Voter
{

    const CREATE = 'create';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /** @var  */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        // TODO: Implement voteOnAttribute() method.

        $user = $token->getUser();

        if(!$user instanceof User)
        {
            return false;
        }

        $sale = $subject;

        switch ($attribute) {
            case self::CREATE:
            case self::EDIT:
            case self::DELETE:
                return (!empty(array_intersect(['ROLE_GERENCIAR_VENDAS', 'ROLE_ADMIN'], $user->getRoles())));

        }

        throw new \LogicException('This code should not be reached!');

    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CREATE, self::DELETE, self::EDIT

        ])){

            return false;

        }

        return $subject instanceof Sale;
        // TODO: Implement supports() method.
    }

}