<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 28/02/19
 * Time: 14:47
 */

namespace AppBundle\Security;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CategoryVoter extends Voter
{

    const CREATE = 'create';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /** @var  */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        // TODO: Implement voteOnAttribute() method.

        $user = $token->getUser();

        if(!$user instanceof User)
        {
            return false;
        }

        $category = $subject;

        switch ($attribute) {
            case self::CREATE:
            case self::EDIT:
                return (!empty(array_intersect(['ROLE_GERENCIAR_PRODUTOS', 'ROLE_ADMIN'], $user->getRoles())));
            case self::DELETE:

                $productCategory = $this->entityManager->getRepository(Category::class)->findBy(['id'=>$category->getId()]);

                if (!empty($productCategory) ){
                    return false;
                }
                else {

                    return (!empty(array_intersect(['ROLE_GERENCIAR_PRODUTOS', 'ROLE_ADMIN'], $user->getRoles())));
                }
        }

        throw new \LogicException('This code should not be reached!');

    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::CREATE, self::DELETE, self::EDIT

        ])){

            return false;

        }

        return $subject instanceof Category;
        // TODO: Implement supports() method.
    }

}