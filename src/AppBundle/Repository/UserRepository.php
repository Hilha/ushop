<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 01/03/19
 * Time: 14:23
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findByRole($role, $role2){
        $qb = $this->createQueryBuilder('u');
        return $qb->where($qb->expr()->like('u.roles', ':role'))
            ->orWhere($qb->expr()->like('u.roles',':role2'))
            ->setParameter('role', trim("%$role%"))
            ->setParameter('role2', trim("%$role2%"))
            ->getQuery()
            ->getResult();
    }

}