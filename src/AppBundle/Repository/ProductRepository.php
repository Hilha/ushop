<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 01/03/19
 * Time: 14:11
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findByStockQtd($qtd){
        try {
            $queryBuilder = $this->createQueryBuilder('p');
            return $queryBuilder->where($queryBuilder->expr()->lte('p.qtdEstoque',':qtd'))
                ->setParameter('qtd', $qtd)
                ->getQuery()
                ->getResult();
        } catch (\Exception $e) {}
    }

}