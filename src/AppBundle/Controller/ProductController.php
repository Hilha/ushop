<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 11:37
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use AppBundle\Security\ProductVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends  Controller
{
    public function indexAction()
    {

        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');

        $products= $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();


        return $this->render("@App/product/index.html.twig" , [
            'products'=>$products
        ]) ;
    }

    public function editAction(Request $request, Product $product)
    {

        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();
            }

            catch (\Exception $e)
            {
                dump($e->getMessage());
                die();
            }

            return $this->redirectToRoute('product_show' , [
                'product' => $product->getId()
            ]);
        }

        return $this->render("@App/product/edit.html.twig", [
            'form' => $form->createView()
        ]);
    }

    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            try
            {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($product);
                $entityManager->flush();
            }
            catch (\Exception $e)
            {
                dump($e->getMessage());
                die();
            }

            return $this->redirectToRoute('product_show', [
                'product' => $product->getId()
            ]); //new Response('Saved new product with id '.$client->getId());

        }

        return $this->render("@App/product/new.html.twig" , [
            'form'=>$form->createView()
        ]);

    }


    public function showAction(Product $product)
    {

        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');

        return $this->render("@App/product/show.html.twig", [
            'product' => $product,
            'delete_form' => $this->createDeleteForm($product)->createView()
        ]);
    }

    /**
     * @IsGranted("ROLE_GERENCIAR_PRODUTOS")
     *
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Product $product)
    {
        $this->denyAccessUnlessGranted(ProductVoter::DELETE, $product);

        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('product_index');
    }

    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('product' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


}