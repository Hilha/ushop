<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 11:37
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Form\ClientType;
use AppBundle\Security\ClientVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class ClientController extends Controller
{

    /**
     * @IsGranted("ROLE_GERENCIAR_CLIENTES")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_CLIENTES');


        $clients = $this->getDoctrine()
            ->getRepository(Client::class)
            ->findAll();

        return $this->render("@App/client/index.html.twig" , [
            'clients'=>$clients
        ]) ;
    }


    public function editAction(Request $request,Client $client)
    {

        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_CLIENTES');


        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try
            {

            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();

            }
            catch (\Exception $e)
            {
                dump($e->getMessage());
                die();
            }

            return $this->redirectToRoute('client_show' , [
                'client' => $client->getId()
            ]);
        }

        return $this->render("@App/client/edit.html.twig", [
            'form' => $form->createView()
        ]);
    }

    public function newAction(Request $request)
    {

        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_CLIENTES');


        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            try
            {
                $cpfIsValid = $this->validaCPF($client->getCpf());

                if($cpfIsValid)
                {
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($client);
                    $entityManager->flush();
                }
                else
                {
                    /** @var TranslatorInterface $translated */
                    $translated = $this->get('translator')->trans('for_subs.cpf.is_invalid', [] , 'AppBundle');
                    $this->addFlash('error', $translated);
                    return $this->redirectToRoute('client_new', [
                        'client' => $client->getId()
                    ]); //new Response('Saved new product with id '.$client->getId());
                }


            }
            catch (\Exception $e)
            {

                /** @var TranslatorInterface $translated */
                $translated = $this->get('translator')->trans('for_subs.cpf.already_exists', [] , 'AppBundle');
                $this->addFlash('error', $translated);
                return $this->redirectToRoute('client_new', [
                    'client' => $client->getId()
                ]); //new Response('Saved new product with id '.$client->getId());
            }

            return $this->redirectToRoute('client_show', [
                'client' => $client->getId()
            ]); //new Response('Saved new product with id '.$client->getId());

        }

        return $this->render("@App/client/new.html.twig" , [
            'form'=>$form->createView()
        ]);
    }

    private function validaCPF($cpf) {

        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

    public function showAction(Client $client, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_CLIENTES');

        return $this->render("@App/client/show.html.twig", [
            'client' => $client,
            'delete_form' => $this->createDeleteForm($client)->createView()

        ]);


    }

    /**
     * @IsGranted("ROLE_GERENCIAR_CLIENTES")
     *
     * @param Request $request
     * @param Client $client
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function deleteAction(Request $request, Client $client)
    {

        $this->denyAccessUnlessGranted(ClientVoter::DELETE, $client);
        $form = $this->createDeleteForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
        }

        return $this->redirectToRoute('client_index');
    }

    private function createDeleteForm(Client $client)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('client_delete', array('client' => $client->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }




}