<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 11:37
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Product;
use AppBundle\Entity\Sale;
use AppBundle\Entity\SaleItemSale;
use AppBundle\Form\SaleItemSaleType;
use AppBundle\Form\SaleType;
use AppBundle\services\EmailNotifyService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

class SaleController extends Controller
{
    /** @var EmailNotifyService $notify */
    private $notify;

    public function __construct(EmailNotifyService $emailNotifyService)
    {
        $this->notify = $emailNotifyService;
    }


    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_VENDAS');
        $sales = $this->getDoctrine()
            ->getRepository(Sale::class)
            ->findAll();

        return $this->render("@App/sale/index.html.twig", [
            'sales' => $sales
        ]);
    }

    public function editAction(Request $request, Sale $sale)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_VENDAS');
        $oldSale = clone $sale;
        $oldSaleItems = $oldSale->getItems();

        foreach ($oldSaleItems as $item){

            $itemQtd = $item->getQtd();
            $estoque = $item->getProducts()->getQtdEstoque();
            $resto = $itemQtd + $estoque;
            $item->getProducts()->setQtdEstoque($resto);

        }

        $form = $this->createForm(SaleType::class, $sale);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            try {
                $total = 0;
                $entityManager = $this->getDoctrine()->getManager();

                /* @var SaleItemSale $item */
                foreach ($oldSaleItems as $item) {

                    $itemQtd = $item->getQtd();

                    if ($itemQtd <= $item->getProducts()->getQtdEstoque() && $itemQtd != 0)
                    {

                        $newEstoque = $item->getProducts()->getQtdEstoque() - $item->getQtd();
                        $item->getProducts()->setQtdEstoque($newEstoque);
                        $productPreco = $item->getProducts()->getPreco();
                        $itemPreco = $itemQtd * $productPreco;
                        $item->setPreco($itemPreco);
                        $total += $itemPreco;

                    }

                    else {
                        if ($itemQtd == 0) {
                            /** @var TranslatorInterface $translated */
                            $translated = $this->get('translator')->trans('for_subs.sale.quantity_zero', [], 'AppBundle');
                            $this->addFlash('error', $translated);
                            return $this->redirectToRoute('sale_new', [
                                'sale' => $sale->getId()
                            ]);
                        }

                        /** @var TranslatorInterface $translated */
                        $translated = $this->get('translator')->trans('for_subs.sale.quantity_etq', [], 'AppBundle');
                        $this->addFlash('error', $translated);
                        return $this->redirectToRoute('sale_edit', [
                            'sale' => $sale->getId()
                        ]);
                    }
                }

                $sale->setTotal($total);
                $entityManager->persist($sale);
                $entityManager->flush();
                $this->notify->notifyLowStock();

            } catch (\Exception $e) {
                /** @var TranslatorInterface $translated */
                $translated = $this->get('translator')->trans('for_subs.sale.quantity_etq', [], 'AppBundle');
                $this->addFlash('error', $translated);

                return $this->redirectToRoute('sale_edit', [
                    'sale' => $sale->getId()
                ]);
            }

            return $this->redirectToRoute('sale_show', [
                'sale' => $sale->getId()
            ]);
        }

        return $this->render("@App/sale/edit.html.twig", [
            'form' => $form->createView(),
            'sale'  => $sale
        ]);
    }

    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_VENDAS');
        $sale = new Sale();
        $form = $this->createForm(SaleType::class, $sale);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            try {
                $total = 0;
                $entityManager = $this->getDoctrine()->getManager();
                $saleItems = $sale->getItems();

                /* @var SaleItemSale $item */
                foreach ($saleItems as $item) {

                    $itemQtd = $item->getQtd();

                    if ($itemQtd <= $item->getProducts()->getQtdEstoque() && $itemQtd != 0)
                    {
                        $estoque = $item->getProducts()->getQtdEstoque();
                        $estoque = $estoque - $itemQtd;
                        $item->getProducts()->setQtdEstoque($estoque);
                        $productPreco = $item->getProducts()->getPreco();
                        $itemPreco = $itemQtd * $productPreco;
                        $item->setPreco($itemPreco);
                        $total += $itemPreco;
                    }
                    else {
                        if ($itemQtd == 0) {
                            /** @var TranslatorInterface $translated */
                            $translated = $this->get('translator')->trans('for_subs.sale.quantity_zero', [], 'AppBundle');
                            $this->addFlash('error', $translated);
                            return $this->redirectToRoute('sale_new', [
                                'sale' => $sale->getId()
                            ]);
                        }

                        /** @var TranslatorInterface $translated */
                        $translated = $this->get('translator')->trans('for_subs.sale.quantity_etq', [], 'AppBundle');
                        $this->addFlash('error', $translated);
                        return $this->redirectToRoute('sale_new', [
                            'sale' => $sale->getId()
                        ]);
                    }
                }

                $sale->setTotal($total);
                $entityManager->persist($sale);
                $entityManager->flush();
                $this->notify->notifyLowStock();
            } catch (\Exception $e) {
                /** @var TranslatorInterface $translated */
                $translated = $this->get('translator')->trans('for_subs.sale.quantity_etq', [], 'AppBundle');
                $this->addFlash('error', $translated);
                return $this->redirectToRoute('sale_new', [
                    'sale' => $sale->getId()
                ]);
            }

            return $this->redirectToRoute('sale_show', [
                'sale' => $sale->getId()
            ]);
        }

        return $this->render("@App/sale/new.html.twig", [
            'form' => $form->createView()
        ]);
    }


    public function showAction(Sale $sale)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_VENDAS');
        return $this->render("@App/sale/show.html.twig", [
            'sale' => $sale,
            'delete_form' => $this->createDeleteForm($sale)->createView()
        ]);
    }

    public function deleteAction(Request $request, Sale $sale)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_VENDAS');

        $entityManager = $this->getDoctrine()->getManager();

        /** @var SaleItemSale $item */
        foreach ($sale->getItems() as $item){

            $itemQtd = $item->getQtd();
            $estoque = $item->getProducts()->getQtdEstoque();
            $resto = $itemQtd + $estoque;
            $item->getProducts()->setQtdEstoque($resto);

        }

        $form = $this->createDeleteForm($sale);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sale);
            $em->flush();
        }

        return $this->redirectToRoute('sale_index');
    }

    private function createDeleteForm(Sale $sale)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sale_delete', array('sale' => $sale->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function calcularAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_VENDAS');
        $quantity = $request->get('quantity');
        $oldQuantity = $request->get('oldQuantity', []);
        $products = $request->get('products');
        $total = 0;
        $resposta = [
            'error' => [],
            'total' => 0
        ];

        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            $qtd = $quantity[$i];
            $oldQtd = $oldQuantity[$i] ?? 0;
            $em = $this->getDoctrine()->getManager();
            $productEntity = $em->getRepository('AppBundle:Product')->findBy(['nome' => $product]);

            /* @var Product $prod */
            foreach ($productEntity as $prod) {
                $prodStock = $prod->getQtdEstoque() + $oldQtd;

                if ($prodStock >= $qtd)
                {
                    $prodPreco = $prod->getPreco();
                    $total += $prodPreco * $qtd;
                } else {
                    /** @var TranslatorInterface $translated */
                    $translated = $this->get('translator')->trans('for_subs.sale.etq_overlimit', [], 'AppBundle');

                    $error = $translated . $product . ' -> qtd max.: (' . $prodStock . ')';
                    array_push($resposta['error'], $error);
                }
            }
        }

        $resposta['total'] = $total;

        return new JsonResponse($resposta);
    }

}