<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 11:37
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use AppBundle\Security\CategoryVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();
        return $this->render("@App/category/index.html.twig" , [
            'categories'=>$categories
        ]) ;
    }

    public function editAction(Request $request, Category $category)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
            }
            catch (\Exception $e)
            {
                dump($e->getMessage());
                die();
            }

            return $this->redirectToRoute('category_show' , [
                'category' => $category->getId()
            ]);
        }
        return $this->render("@App/category/edit.html.twig", [
            'form' => $form->createView()
        ]);
    }

    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            try
            {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($category);
                $entityManager->flush();
            }
            catch (\Exception $e)
            {
                dump($e->getMessage());
                die();
            }
            return $this->redirectToRoute('category_show', [
                'category' => $category->getId()
            ]); //new Response('Saved new product with id '.$client->getId());
        }
        return $this->render("@App/category/new.html.twig" , [
            'form'=>$form->createView()
        ]);
    }

    public function newcatAction (Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');
        $category = new Category();
        $category->setNome($request->request->get('newCategory'));
        $newCat = [];

        try
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            $newCat['id'] = $category->getId();
        }
        catch (\Exception $e)
        {
            $newCat['msg'] = 'deu ruim';
            return new JsonResponse($newCat);
        }

        $newCat['msg'] = 'deu bom';
        return new JsonResponse($newCat);
    }

    public function showAction(Request $request, Category $category)
    {
        $this->denyAccessUnlessGranted('ROLE_GERENCIAR_PRODUTOS');

        return $this->render("@App/category/show.html.twig", [
           'category' => $category,
            'delete_form' => $this->createDeleteForm($category)->createView()
        ]);
    }

    /**
     * @IsGranted("ROLE_GERENCIAR_PRODUTOS")
     *
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {

        $this->denyAccessUnlessGranted(CategoryVoter::DELETE, $category);

        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('category_index');
    }

    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('category' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


}