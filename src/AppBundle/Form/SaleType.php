<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 13:50
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class SaleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('client', EntityType::class, [
                'class' => 'AppBundle:Client',
                'choice_label' => function($client) {
                    return $client->getNome();
                }

            ])

            ->add('date' , DateType::class , [
                'format' => 'dd-MM-yyyy',
                'required'=> true
            ])
            ->add('total', MoneyType::class , [
                'currency'=>'BRL',
                'scale'=> 2,

            ])

            ->add('items' , CollectionType::class, [
                'label' => ' ',
                'entry_type' => SaleItemSaleType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true
            ])


            ->add('SALVAR' , SubmitType::class)


        ;
    }

}