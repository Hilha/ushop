<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 13:50
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder
            ->add('nome' , TextType::class , [
                'required'=> true
            ])
            ->add('salvar' , SubmitType::class)
        ;
    }


}