<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 20/02/19
 * Time: 08:30
 */

namespace AppBundle\Form;


use AppBundle\Entity\SaleItemSale;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SaleItemSaleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('products' , EntityType::class, [
            'class' => 'AppBundle:Product',
            'choice_label' => function($product) {
                return $product->getNome();

        }]);
        $builder->add('qtd');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SaleItemSale::class,
        ]);
    }


}