<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/02/19
 * Time: 13:50
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome' , TextType::class , [
                'required'=> true
            ])
            ->add('preco', MoneyType::class , [
                'currency'=>'BRL',
                'scale'=> 2,
                'required'=> true
            ])

            ->add('qtdEstoque' , TextType::class , [
                'required'=> true
            ])

           ->add('categories', EntityType::class, [
               'class' => 'AppBundle:Category',
               'choice_label' => function($category) {
                    return $category->getNome();
               },
               'multiple' => true
           ])



            ->add('SALVAR' , SubmitType::class)

        ;
    }


}