<?php

namespace AppBundle\Entity;

/**
 * SaleItemSale
 */
class SaleItemSale
{
    /**
     * @var string
     */
    private $preco;

    /**
     * @var integer
     */
    private $qtd;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $products;


    /**
     * Set preco
     *
     * @param string $preco
     *
     * @return SaleItemSale
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;

        return $this;
    }

    /**
     * Get preco
     *
     * @return string
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * Set qtd
     *
     * @param integer $qtd
     *
     * @return SaleItemSale
     */
    public function setQtd($qtd)
    {
        $this->qtd = $qtd;

        return $this;
    }

    /**
     * Get qtd
     *
     * @return integer
     */
    public function getQtd()
    {
        return $this->qtd;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set products
     *
     * @param \AppBundle\Entity\Product $products
     *
     * @return SaleItemSale
     */
    public function setProducts(\AppBundle\Entity\Product $products = null)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Get products
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProducts()
    {
        return $this->products;
    }
}
